version="1.10.*"
tags={
	"Alternative History"
}
name="Neuordnung: Dark Days"
supported_version="1.10.3"
replace_path="common/bookmarks"
replace_path="common/ideologies"
replace_path="history/states"
replace_path="map"