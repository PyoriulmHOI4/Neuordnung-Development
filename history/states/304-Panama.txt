
state={
	id=304
	name="STATE_304"
	manpower = 467459
	resources={
		oil=1 # was: 1
	}
	
	state_category = town
	
	history={
		owner = CTA
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 2 
			air_base = 1
			7630 = {
				naval_base = 4
			}
			4611 = {
				naval_base = 1
			}
		}
		add_core_of = CTA
		victory_points = {
			7630 5 
		}
		victory_points = {
			4611 1
		}

	}

	provinces={
		1633 4611 4624 
	}
}
