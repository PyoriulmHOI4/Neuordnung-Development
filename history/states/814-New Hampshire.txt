state={
	id=814
	name="STATE_814" # Northern Kashmir
	manpower = 1415800
	
	state_category = rural

	history={
		owner = USA
		buildings = {
			infrastructure = 2
		}
		add_core_of = USA
	}

	provinces={
		733 3712 3715 10481
	}
}