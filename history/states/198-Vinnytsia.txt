
state={
	id=198
	name="STATE_198"
	manpower = 1893391

	state_category = large_town

	history={
		victory_points = {
			476 3
		}
		owner = ROM
		buildings = {
			infrastructure = 5
			air_base = 3
		}
		add_core_of = ROM
	}

	provinces={
		462 476 3430 6429 6455 9423 9435 9576 
	}
}
