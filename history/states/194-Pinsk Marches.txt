
state={
	id=194
	name="STATE_194" # Mozyr
	manpower = 934632
	
	state_category = rural

	history={
		owner = OTL
		buildings = {
			infrastructure = 3
			air_base = 3
		}
		victory_points = {
			6232 1
		}
		add_core_of = OTL
	}

	provinces={
		215 3293 
	}
}
